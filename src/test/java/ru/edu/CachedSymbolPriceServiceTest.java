package ru.edu;

import org.junit.Test;
import ru.edu.model.Symbol;

import static org.junit.Assert.*;

public class CachedSymbolPriceServiceTest {

    @Test
    public void getPrice() throws InterruptedException {

        SymbolPriceService cachedSymbolPriceService = new CachedSymbolPriceService(new BinanceSymbolPriceService());

        Symbol firstTestSymbol = cachedSymbolPriceService.getPrice("BTCRUB");

        assertNotNull(firstTestSymbol);

        Thread.sleep(8000L);

        Symbol secondTestSymbol = cachedSymbolPriceService.getPrice("BTCRUB");

        assertNotNull(secondTestSymbol);
        assertTrue(firstTestSymbol.getTimeStamp().isBefore(secondTestSymbol.getTimeStamp()));
    }
}