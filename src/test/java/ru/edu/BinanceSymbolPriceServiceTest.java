package ru.edu;

import org.junit.Test;
import ru.edu.model.Symbol;

import static org.junit.Assert.*;

public class BinanceSymbolPriceServiceTest {

    @Test
    public void getPrice() {
        SymbolPriceService symbolPriceService = new BinanceSymbolPriceService();
        Symbol price = symbolPriceService.getPrice("BTCUSDT");

        assertNotNull(price);
    }
}