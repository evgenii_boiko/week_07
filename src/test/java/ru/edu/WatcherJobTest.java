package ru.edu;

import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class WatcherJobTest {

    @Test
    public void run() throws InterruptedException {

        SymbolPriceService symbolPriceService = new BinanceSymbolPriceService();
        SymbolPriceService cachedSymbolPriceService = new CachedSymbolPriceService(symbolPriceService);

        String[] symbols = {"BTCUSDT", "BTCUSDT", "BTCRUB"};
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(symbols.length);

        for (String symbol : symbols) {

            executorService.scheduleAtFixedRate(new WatcherJob(symbol, cachedSymbolPriceService), 0, 1, TimeUnit.SECONDS);
        }

        Thread.sleep(10_000);

        executorService.shutdown();
        assertTrue(executorService.isShutdown());
    }
}