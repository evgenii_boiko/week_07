package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Реализация интерфейса получения информации о курсе обмена.
 */
public class SymbolImpl implements Symbol {

    // Возможно, нужно использовать конструктор - время около 44:30

    /**
     * Название котировки.
     */
    private String symbol;

    /**
     * Цена котировки.
     */
    private BigDecimal price;


    /**
     * Метка времени.
     */
    private Instant timeStamp = Instant.now();

    /**
     * Конструктор по-умолчанию.
     */
    public SymbolImpl() {
    }

    /**
     * Конструктор с параметрами.
     *
     * @param symbolInc - название котировки.
     * @param priceInc - цена котировки.
     * @param timeStampInc - метка времени.
     */
    public SymbolImpl(final String symbolInc,
                      final BigDecimal priceInc,
                      final Instant timeStampInc) {
        this.symbol = symbolInc;
        this.price = priceInc;
        this.timeStamp = timeStampInc;
    }


    /**
     * Получение названия котировки.
     *
     * @return symbol - название котировки.
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * Получение цены котировки.
     *
     * @return price - цена котировки.
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Получение времени получения данных котировки.
     *
     * @return timeStamp - время получения данных котировки.
     */
    @Override
    public Instant getTimeStamp() {
        return timeStamp;
    }

    /**
     * Задание названия котировки.
     *
     * @param symbolInc - название котировки
     */
    public void setSymbol(final String symbolInc) {
        this.symbol = symbolInc;
    }

    /**
     * Задание цены котировки.
     *
     * @param priceInc - цена котировки
     */
    public void setPrice(final BigDecimal priceInc) {
        this.price = priceInc;
    }

    /**
     * Задание времени получения данных котировки.
     *
     * @param timeStampInc - время получения данных котировки.
     */
    public void setTimeStamp(final Instant timeStampInc) {
        this.timeStamp = timeStampInc;
    }

    /**
     * Метод toString.
     *
     * @return значения объекта котировки.
     */
    @Override
    public String toString() {
        return "SymbolImpl{"
                + "symbol='" + symbol + '\''
                + ", price=" + price
                + ", timeStamp=" + timeStamp
                + '}';
    }
}
