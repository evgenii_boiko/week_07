package ru.edu;

import ru.edu.model.Symbol;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс-реализация сервиса кэширования данных.
 */
public class CachedSymbolPriceService implements SymbolPriceService {

    /**
     * Делегат.
     *
     * Мы делегируем ему получение данных,
     * если их не оказалось в кэше, либо они отсутствуют.
     */
    private final SymbolPriceService delegate;

    /**
     *
     */
    private final Map<String, Symbol> cache =
            Collections.synchronizedMap(new HashMap<>());

    /**
     * Время ожидания обновления кэша.
     */
    public static final int CASH_TIME_AWAITING = 5;

    /**
     * Конструктор для объекта кэширования.
     *
     * @param delegateInc - делегат.
     */
    public CachedSymbolPriceService(final SymbolPriceService delegateInc) {
        this.delegate = delegateInc;
    }

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш,
     * с помощью которого данные будут обновляться не чаще, чем раз в 10 секунд.
     *
     * Данный метод блокируемый (synchronized):
     * только один поток может к нему обратиться
     * и получить данные в одно время - чтобы не было гонки состояний.
     *
     * @param symbolName - название котировки.
     * @return null или объект котировки.
     */
    @Override
    public Symbol getPrice(final String symbolName) {

        synchronized (symbolName) {

            if (!cache.containsKey(symbolName)
                    || Instant.now()
                    .minus(CASH_TIME_AWAITING, ChronoUnit.SECONDS)
                    .isAfter(cache.get(symbolName).getTimeStamp())) {
                cache.put(symbolName, delegate.getPrice(symbolName));
            }
        }
        return cache.get(symbolName);
    }
}
