package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;

/**
 * Класс Watcher.
 */
public class WatcherJob implements Runnable {

    /**
     * Передаем текущий класс Watcher
     * в метод getLogger.
     *
     * Таким образом, из данного класса через объект LOGGER
     * в дальнейшем будем передавать события.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(WatcherJob.class);

    /**
     * Символ, за которым наблюдаем.
     */
    private final String symbolToWatch;

    /**
     * Объект сервиса.
     */
    private final SymbolPriceService service;

    /**
     * Символ.
     * Это - предыдущий ответ сервера по котировке (нашему запросу).
     */
    private Symbol lastData = null;

    /**
     * Конструктор.
     * Получает данные о символе, за которым наблюдаем,
     * и об объекте сервиса извне.
     *
     * @param symbolToWatchImpl - символ, за которым наблюдаем.
     * @param serviceImpl - объект сервиса.
     */
    public WatcherJob(final String symbolToWatchImpl,
                      final SymbolPriceService serviceImpl) {
        this.symbolToWatch = symbolToWatchImpl;
        this.service = serviceImpl;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        Symbol symbol = service.getPrice(symbolToWatch);
        LOGGER.info("Received the answer: {}."
                + "Previous answer: {}", symbol, lastData);
        lastData = symbol;
    }
}
