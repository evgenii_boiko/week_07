package ru.edu;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;

import java.time.Instant;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SymbolPriceServiceTest {

    /**
     * Передаем текущий класс SymbolPriceServiceTest
     * в метод getLogger.
     *
     * Таким образом, из данного класса через объект LOGGER
     * в дальнейшем будем передавать события.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SymbolPriceServiceTest.class);

    /**
     * Добавляем в тест новый тестовый логгер с новым именем.
     *
     * Имя задано наше - это не имя класса.
     */
    private static final Logger LOGGER_NAMED_TEST = LoggerFactory.getLogger("My test-named Logger");

    public static final String SYMBOL_NAME = "MyTestValue";

    private SymbolPriceService service;

    private Symbol firstMockSymbol = mock(Symbol.class);
    private Symbol secondMockSymbol = mock(Symbol.class);

    @Test
    public void getPrice() throws InterruptedException {

        /**
         * Примерный тест проверки работы кэша.
         *
         * Делаем 2 вызова и смотрим, что данные были получены в одно время.
         * После ожидания таймаута жизни данных в кэше делаем повторный запрос и проверяем,
         * что данные имеют метку времени, полученную после сброса.
         */
        service = mock(SymbolPriceService.class);

        when(service.getPrice(SYMBOL_NAME)).thenReturn(firstMockSymbol);
        when(firstMockSymbol.getTimeStamp()).thenReturn(Instant.now());

        Symbol price = service.getPrice(SYMBOL_NAME);
        Symbol priceAgain = service.getPrice(SYMBOL_NAME);
        assertEquals(price.getTimeStamp(), priceAgain.getTimeStamp());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("My test debug message: {}", price);
        }

        long timeout = 11_000L;

        LOGGER.info("Ждем сброса кэша: {} сек ", timeout);
        Thread.sleep(timeout);

        when(service.getPrice(SYMBOL_NAME)).thenReturn(secondMockSymbol);
        when(secondMockSymbol.getTimeStamp()).thenReturn(Instant.now());

        Symbol priceNew = service.getPrice(SYMBOL_NAME);
        assertTrue(price.getTimeStamp().isBefore(priceNew.getTimeStamp()));
    }
}