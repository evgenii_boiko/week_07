package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс получения информации о курсе обмена.
 */
public interface Symbol {

    /**
     * Получение названия котировки.
     *
     * @return symbol - название котировки.
     */
    String getSymbol();

    /**
     * Получение цены котировки.
     * @return price - цена котировки.
     */
    BigDecimal getPrice();

    /**
     * Получение времени получения данных котировки.
     *
     * @return timeStamp - время получения данных котировки.
     */
    Instant getTimeStamp();
}
