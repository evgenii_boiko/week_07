package ru.edu.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static org.junit.Assert.*;

public class SymbolImplTest {

    SymbolImpl symbolImpl = new SymbolImpl();

    @Test
    public void test() {

        String s = "USDBTC";
        symbolImpl = new SymbolImpl(s, new BigDecimal("10"), Instant.now());

        assertNotNull(symbolImpl);
        Assert.assertNotNull(symbolImpl);
        assertEquals(s, symbolImpl.getSymbol());
    }

    @Test
    public void getSymbol() {

        String s = "USDBTC";
        symbolImpl.setSymbol(s);

        assertEquals(s, symbolImpl.getSymbol());
    }

    @Test
    public void getPrice() {

        BigDecimal price = new BigDecimal("500");
        symbolImpl.setPrice(price);

        assertEquals(price, symbolImpl.getPrice());
    }

    @Test
    public void getTimeStamp() {

        Instant instant = Instant.now();
        symbolImpl.setTimeStamp(instant);

        assertEquals(instant, symbolImpl.getTimeStamp());
    }

    @Test
    public void testToString() {

        Instant instant = Instant.now();
        symbolImpl = new SymbolImpl("BTC", new BigDecimal(500), instant);

        assertNotNull(symbolImpl.toString());
    }
}