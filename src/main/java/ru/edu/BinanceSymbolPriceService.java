package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;

/**
 *
 */
public class BinanceSymbolPriceService implements SymbolPriceService {

    /**
     * Передаем текущий класс BinanceSymbolPriceService
     * в метод getLogger.
     *
     * Таким образом, из данного класса через объект LOGGER
     * в дальнейшем будем передавать события.
     */
    private static final Logger LOGGER = LoggerFactory.
            getLogger(BinanceSymbolPriceService.class);

    /**
     * Создаем объект restTemplate.
     */
    private RestTemplate restTemplate = new RestTemplate();


    /**
     * URL сервиса api.binance.com.
     */
    public static final String API_BINANCE_URL =
            "https://api.binance.com/api/v3/ticker/price";

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого
     * данные будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName - название котировки.
     * @return null или объект котировки.
     */
    @Override
    public Symbol getPrice(final String symbolName) {

        ResponseEntity<SymbolImpl> response =
                restTemplate.getForEntity(API_BINANCE_URL
                        + "?symbol=" + symbolName, SymbolImpl.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            LOGGER.error("Haven't got the answer from the server."
                    + "The status code is: {}", response.getStatusCode());
            return null;
        }

        SymbolImpl symbol = response.getBody();
        LOGGER.debug("Received the response for {}: {}", symbolName, symbol);
        return symbol;
    }
}
